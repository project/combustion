(This theme has been ported to Drupal 5.x by ThemeBot, http://themebot.com)  Thank you for downloading downloading Combustion. At the moment there's not much in this README document as I have yet to finalise it. Combustion is a theme for Drupal v4.7 that was developed in my spare time, so if any bugs/anomalies are discovered then please report them through the proper channels. It would also be greatly appreciated if anyone feels the need to fix any bugs that this theme might contain!

--
Phobos Gekko

Gekko's Lair of Fire
http://phobos.redfyre.net



NOTES
----------------------------
If you want the subjects to be displayed on comments, then replace the following code in comment.tpl.php:

********************************
  <!-- [SUBJECT] -->
********************************

With:

********************************
  <tr>
    <td colspan="2" class="title"><?php print $title ?></td>
  </tr>
********************************