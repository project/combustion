<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body <?php print theme("onload_attribute"); ?>>
<table border="0" cellpadding="0" cellspacing="0" id="wrapper">
  <tr>
    <td colspan="2" class="header_img">
<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
<?php if ($site_slogan) : ?>
<div class="site-slogan"><?php print($site_slogan) ?></div>
<?php endif;?>
</td>
  </tr>
  <tr>
    <td colspan="2" class="navbar">
<div class="primarylinks">
<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>  
</div>
</td>
  </tr>
  <tr>
    <td class="main-content">
<div class="primary-container">
<?php if ($breadcrumb != ""): ?>
<?php print $breadcrumb ?>
<?php endif; ?>
<?php if ($messages != ""): ?>
  <div id="message"><?php print $messages ?></div>
<?php endif; ?>
<?php if ($title != ""): ?> 
  <div align="left"><h2 class="page-title"><?php print $title ?></h2></div> 
<?php endif; ?> 
<?php if ($tabs != ""): ?> 
<?php print $tabs ?> 
<?php endif; ?>
<?php if ($help != ""): ?>
  <p id="help"><?php print $help ?></p>
<?php endif; ?>
  
  <?php print($content) ?>
  
</div>
  </td>
<td class="sidebar">
  <?php print $sidebar_right ?>
  <?php print $sidebar_left ?></td>
  </tr>
  <tr>
    <td colspan="2" class="footer_img">
    <div class="footer">
    <?php if ($footer_message) : ?>
	<?php print $footer_message;?>
	<?php endif; ?>
	<p><i>Combustion</i> designed by <a href="http://phobos.redfyre.net/">Phobos Gekko</a> | Ported to Drupal 5.0 by <a href="http://themebot.com/">ThemeBot</a>

	</p>
</div></td>
  </tr>
</table>
  <?php print $closure; ?>
</body>
</html>
