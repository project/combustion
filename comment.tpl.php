<table width="100%" class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>">
  <!-- [SUBJECT] -->
  <tr>
	<td class="comment_info">
	<div class="author">Author: <?php print $author ?></div>
	<div class="comment_date"><?php print $date ?></div>
	</td>
    <td class="comment_picture"><?php print $picture ?></td>
  </tr>
  
  <tr>
    <td colspan="3" class="comment_content">
	<?php if ($comment->new) : ?>
	  <a id="new"></a>
	  <span class="new"><?php print $new ?></span>
	<?php endif; ?>
	<?php print $content ?>	</td>
  </tr>
  <tr>
    <td colspan="3" class="links"><?php print $links ?></td>
  </tr>
</table>
