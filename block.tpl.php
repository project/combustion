<div class="block block-<?php print $block->module ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>">
<table>
  <tr>
    <td class="block_header"><?php print $block->subject ?></td>
  </tr>
  <tr>
    <td class="block_content_background">
	  <div class="block_content">
		<?php print $block->content ?>
	  </div>
	</td>
  </tr>
  <tr>
    <td class="block_footer"></td>
  </tr>
</table>
</div>